

// The InfluenceOracle class is an oracle that gives the influence of a node
// Influence is computed by keeping track of a set of cascades
// and computing average reachability over them
class InfluenceOracle {


  Graph* realizations; // random realizations of the influence graph to estimate f

  int num_realizations; // the number of realizations stored at realizations


private:


  // Compute the random realizations of the graph and save in this->realizations
  // done multithreaded
  void computeRealizations(ProbabilisticGraph& graph) {

      // Allocate space for the realizations
      this->realizations = new Graph [this->num_realizations];

      // Generate all of the realizations (multithreaded)
      computeRealizationsMultithreaded(graph, this->num_realizations, this->realizations);

  }


public:


  // Approximates the influence by averaging reachability over num_realizations realizations of the graph
  InfluenceOracle(ProbabilisticGraph& graph, int num_realizations): num_realizations(num_realizations) {

    // Compute the realizations that we will be estimating influence over
    this->computeRealizations(graph);
  }
  // END


  ~InfluenceOracle() {
      delete[] this->realizations;
  }
  // END


  int getNumRealizations() {
    return this->num_realizations;
  }
  // END


  // Compute F on a set of nodes
  // Pass in both a membership vector and a vector of ids to make computations easier
  // Multithreaded
  double operator()(vector<bool>& nodes, vector<int>& node_ids) {

    return computeAvgReachabilityMultithreaded(this->realizations, this->num_realizations,
      nodes, node_ids);

  }
  // END


  // Compute the marginal gain of adding each element in the graph to nodes
  // Return a vector of the estimated marginal gain for each node
  vector<double> deltaF(vector<bool>& nodes, vector<int>& node_ids) {

    return computeDeltaReachabilityMultithreaded(this->realizations, this->num_realizations,
      nodes, node_ids);
  }
  // END


};

















































//
