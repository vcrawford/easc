

// The InfluenceOracle class is an oracle that gives the influence of a node
// Influence is computed by keeping track of reverse reachability sets
// and computing the intersection
class InfluenceOracleRIS {


  bitset<GRAPH_SIZE>* rr_sets; // reverse reachable sets
  int N; // number of rr_sets

  // Keep track of a set A to make computations faster
  // Maps rr_set (in order of appearance in rr_sets) to whether it intersects with A
  //vector<bool> rr_sets_hit;

private:


  // Compute the rr_sets
  void computeRRSets(ProbabilisticGraph& graph) {

      // Generate all of the rr_sets
      this->rr_sets = new bitset<GRAPH_SIZE> [this->N];
      computeRRSetsMultithreaded(graph, this->N, this->rr_sets);

  }


public:



  InfluenceOracleRIS() {}



  void init(ProbabilisticGraph& graph, int N) {

    this->N = N;
    this->computeRRSets(graph);

  }
  // END



  // Load the oracle from a file
  void init(string& filename, int N) {

    this->N = N;
    this->rr_sets = new bitset<GRAPH_SIZE> [this->N];

    ifstream in;
    in.open(filename);

    string line;
    int line_number = 0;

    cout << "\r" << setw(10) << 0 << "%" << flush;
    for (int i = 0; i < N; i++) {

      assert(getline(in, line));
      stringstream line_strm;
      line_strm.str(line);

      int x;
      int elt_number = 0;

      while (line_strm >> x) {

		  this->rr_sets[line_number][x] = true;
      }

      line_number++;
      cout << "\r" << setw(10) << int(100*(double(line_number)/this->N)) << "% ";
      cout << flush;

    }
    cout << endl;

    in.close();

  }



  // Save the oracle to a file to be reloaded later
  void save(string& filename) {

    ofstream out;
    out.open(filename);

    cout << "\r" << setw(10) << 0 << "%" << flush;
    for (int i = 0; i < this->N; i++) {
      for (int j = 0; j < GRAPH_SIZE; j++) {
		  if (this->rr_sets[i][j]) out << j << " ";
      }
      out << endl;
      cout << "\r" << setw(10) << int(100*(double(i)/this->N)) << "% ";
      cout << flush;
    }

    cout << endl;
    out.close();

  }



  // Compute f on a set of nodes
  // Multithreaded
  double operator()(bitset<GRAPH_SIZE>& nodes) {

    int num_intersections = countIntersectionsMultithreaded(this->rr_sets, this->N, nodes);

    return (float(GRAPH_SIZE)/this->N)*num_intersections;

  }
  // END



  // Compute f on a set of nodes
  // Multithreaded
  double operator()(bitset<GRAPH_SIZE>& nodes, double threshold) {

    int num_intersections = countIntersectionsMultithreaded(this->rr_sets, this->N, nodes);

    return min(threshold, (double(GRAPH_SIZE)/this->N)*num_intersections);

  }
  // END



  // Compute the marginal gain of adding each element in the graph to chosen
  vector<double> deltaF(bitset<GRAPH_SIZE>& chosen, double current_f) {

    vector<double> deltaF (GRAPH_SIZE, 0.0);

    for (int j = 0; j < GRAPH_SIZE; j++) {

      if (!chosen[j]) {
        chosen[j] = true;
        deltaF[j] = this->operator()(chosen) - current_f;
        chosen[j] = false;
      }

    }


    return deltaF;

  }
  // END



  // Compute the marginal gain of adding each element in the graph to chosen
  vector<double> deltaF(bitset<GRAPH_SIZE>& chosen, double current_f, double threshold) {

	 assert(current_f <= threshold);

    vector<double> deltaF (GRAPH_SIZE, 0.0);

    for (int j = 0; j < GRAPH_SIZE; j++) {

      if (!chosen[j]) {
        chosen[j] = true;
        deltaF[j] = min(threshold, this->operator()(chosen)) - current_f;
        chosen[j] = false;
      }

    }


    return deltaF;

  }
  // END




};

















































//
