// Functions for reachability computations over a set of graph realizations
// To be used for influence oracles


// Graph realizations are spread equally among threads
// How many realizations should each thread deal with
// (except the last)
int countPerThread(int num_realizations) {

  return num_realizations/NUM_THREADS;
}
// END


// Graph realizations are spread among threads
// How many realizations should the last thread deal with
int countLastThread(int num_realizations) {

  return num_realizations-(NUM_THREADS-1)*countPerThread(num_realizations);
}
// END



// Compute k random reverse reachable sets
// Store the rr sets in rr_sets
// Assumes that memory is already allocated
// Pass graph by copy to avoid any issues with multithreading
void computeRRSets(ProbabilisticGraph graph, int k, bitset<GRAPH_SIZE>* rr_sets) {

  mt19937 gen;
  uniform_real_distribution<double> dist; // for coin flips over edges
  uniform_int_distribution<int> dist2 (0, graph.getNodeCount()-1); // for picking random target node
  random_device rd;
  gen.seed(rd());

  for (int i = 0; i < k; i++) {
    rr_sets[i] = graph.sampleRRSet(dist2(gen), gen, dist);
  }
}
//END



// Compute k random reverse reachable sets
// Store the rr sets in rr_sets
// Assumes that memory is already allocated
// Does multithreaded calls to computeRRSets
void computeRRSetsMultithreaded(ProbabilisticGraph& graph, int k,
    bitset<GRAPH_SIZE>* rr_sets) {

    // Multithreaded
    thread* threads [NUM_THREADS - 1];
    int rr_sets_per_thread = countPerThread(k);
    int rr_sets_last_thread = countLastThread(k);

    // Start each thread
    int memory_index = 0;
    for (int i = 0; i < NUM_THREADS - 1; i++) {
      // Here is a single thread
      threads[i] = new thread(computeRRSets, graph, rr_sets_per_thread, &rr_sets[memory_index]);
      memory_index += rr_sets_per_thread;
    }

    // Last thread is here
    computeRRSets(graph, rr_sets_last_thread, &rr_sets[memory_index]);

    for (int i = 0; i < NUM_THREADS - 1; i++) {
      threads[i]->join();
    }
}
// END



// Compute the number of times elts intersects with a set in rr_sets
// k sets in rr_sets
// Store number in intersect
// elts is pass by copy to avoid issues with multithreading
void countIntersections(bitset<GRAPH_SIZE>* rr_sets, int k, bitset<GRAPH_SIZE> elts,
  int* intersect) {

  *intersect = 0;

  for (int i = 0; i < k; i++) {
    // Do we intersect set i?
    if ((rr_sets[i]&elts).any()) (*intersect)++;
  }

}
//END



// Compute the number of times elts intersects with a set in rr_sets
// k sets in rr_sets
// Store number in intersect
// Calls countIntersections in different threads
// Returns the number of intersections
int countIntersectionsMultithreaded(bitset<GRAPH_SIZE>* rr_sets, int k,
  bitset<GRAPH_SIZE>& elts) {

  // hold each thread's intersection count
  int intersects [NUM_THREADS];

  // Multithreaded
  thread* threads [NUM_THREADS - 1];
  int memory_index = 0;
  int rr_sets_per_thread = countPerThread(k);
  int rr_sets_last_thread = countLastThread(k);

  // Start each thread
  for (int i = 0; i < NUM_THREADS - 1; i++) {
    // Here is a single thread
    threads[i] = new thread(countIntersections, &rr_sets[memory_index],
      rr_sets_per_thread, elts, &intersects[i]);
    memory_index += rr_sets_per_thread;
  }
  // Last thread is here
  countIntersections(&rr_sets[memory_index], rr_sets_last_thread, elts,
    &intersects[NUM_THREADS-1]);

  for (int i = 0; i < NUM_THREADS - 1; i++) {
    threads[i]->join();
  }

  // Now add up the intersection count over all of the sets
  int intersects_total = 0;
  for (int i = 0; i < NUM_THREADS; i++) {
    intersects_total += intersects[i];
  }

  return intersects_total;
}
//END



// Compute k random realizations of probabilistic graphs
// Store the graphs in memory starting at realizations
// Assumes that memory is already allocated, just copies graphs in
// Pass by copy to avoid any issues with multithreading
void computeRealizations(ProbabilisticGraph graph, int k, Graph* realizations, int r) {

  mt19937 gen;
  uniform_real_distribution<double> dist;
  random_device rd;
  gen.seed(rd());
  //gen.seed(r);

  for (int i = 0; i < k; i++) {
    realizations[i] = graph.sampleRealization(gen, dist);
  }
}
//END



// Compute k random realizations of the graph and store in realizations
// Assumes that space for realizations is already allocated
// Creates threads of computeRealizations
void computeRealizationsMultithreaded(ProbabilisticGraph& graph, int k, Graph* realizations) {

    // Multithreaded
    thread* threads [NUM_THREADS - 1];
    int realizations_per_thread = countPerThread(k);
    int realizations_last_thread = countLastThread(k);

    // Start each thread
    int memory_index = 0;
    for (int i = 0; i < NUM_THREADS - 1; i++) {
      // Here is a single thread
      threads[i] = new thread(computeRealizations, graph, realizations_per_thread,
        &realizations[memory_index], i);

      memory_index += realizations_per_thread;
    }
    // Last thread is here
    computeRealizations(graph, realizations_last_thread, &realizations[memory_index], NUM_THREADS-1);

    for (int i = 0; i < NUM_THREADS - 1; i++) {
      threads[i]->join();
    }
}
// END



// Compute the average reachability of the set nodes over k graph realizations, starting
// at the pointer realizations
// Pass by copy to avoid any issues with multithreading
// Stores the average reachability at reachability over the realizations
void computeAvgReachability(Graph* realizations, int k, vector<bool> nodes, vector<int> node_ids,
  double* reachability) {

  *reachability = 0.0;

  for (int i = 0; i < k; i++) {
    // get reachability count from realization i
    *reachability += realizations[i].countReachable(nodes, node_ids);
  }

  *reachability /= k;
}
//END


// Computes the average reachability of the nodes over the k graph realizations
// Returns average reachability
// Is multithreaded, calls computeAvgReachability as a subroutine
double computeAvgReachabilityMultithreaded(Graph* realizations, int k, vector<bool>& nodes,
  vector<int>& node_ids) {

  // hold each thread's reachability average
  double reachable [NUM_THREADS];

  // Multithreaded
  thread* threads [NUM_THREADS - 1];
  int memory_index = 0;
  int realizations_per_thread = countPerThread(k);
  int realizations_last_thread = countLastThread(k);

  // Start each thread
  for (int i = 0; i < NUM_THREADS - 1; i++) {
    // Here is a single thread
    threads[i] = new thread(computeAvgReachability, &realizations[memory_index],
      realizations_per_thread, nodes, node_ids, &reachable[i]);
    memory_index += realizations_per_thread;
  }
  // Last thread is here
  computeAvgReachability(&realizations[memory_index], realizations_last_thread,
    nodes, node_ids, &reachable[NUM_THREADS-1]);

  for (int i = 0; i < NUM_THREADS - 1; i++) {
    threads[i]->join();
  }

  // Now add up the reachability over all of the realizations
  double avg_reachability = 0.0;
  for (int i = 0; i < NUM_THREADS; i++) {
    avg_reachability += reachable[i];
  }

  return avg_reachability/NUM_THREADS;
}
//END


// Compute the average change in reachability of adding each node to a set of nodes for each realization
// realizations is a pointer to the beginning of the k realizations
// avgDeltaReachability is where we will store each average reachability in order of node id
// Assumes space for finalDeltaReachability is already allocated
void computeDeltaReachability(Graph* realizations, int k, double* avgDeltaReachability,
  vector<bool> nodes, vector<int> node_ids) {

  assert(k > 0);

  int n = realizations[0].getNodeCount();

  // Set to 0
  for (int i = 0; i < n; i++) {
    avgDeltaReachability[i] = 0.0;
  }

  for (int j = 0; j < k; j++) {
    // realization j

    // compute what is already reachable
    vector<bool> reachable_nodes;
    vector<int> reachable_node_ids;
    realizations[j].getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

    // Now go and find delta reachable for every node
    for (int i = 0; i < n; i++) {
      avgDeltaReachability[i] += realizations[j].countNewReachable(i, reachable_nodes);
    }
  }

  // Compute average
  for (int i = 0; i < n; i++) {
    avgDeltaReachability[i] /= k;
  }
}
//END


// Compute the average change in reachability of adding each node to a set of nodes for each realization
// realizations is a pointer to the beginning of the k realizations
// Returns a vector that takes node id to average reachability
vector<double> computeDeltaReachabilityMultithreaded(Graph* realizations, int k, vector<bool>& nodes,
  vector<int>& node_ids) {

  assert(k > 0);

  int n = realizations[0].getNodeCount();

  // Average reachability for every element, in every thread
  double* avgDeltaReachable[NUM_THREADS];
  for (int i = 0; i < NUM_THREADS; i++) {
    avgDeltaReachable[i] = new double [n];
  }

  // Multithreaded
  thread* threads [NUM_THREADS - 1];
  int realizations_per_thread = countPerThread(k);
  int realizations_last_thread = countLastThread(k);

  // Start each thread
  int memory_index = 0;
  for (int i = 0; i < NUM_THREADS - 1; i++) {
    // Here is a single thread
    threads[i] = new thread(computeDeltaReachability, &realizations[memory_index],
      realizations_per_thread, avgDeltaReachable[i], nodes, node_ids);

    memory_index += realizations_per_thread;
  }

  // Last thread is here
  computeDeltaReachability(&realizations[memory_index], realizations_last_thread,
    avgDeltaReachable[NUM_THREADS-1], nodes, node_ids);

  for (int i = 0; i < NUM_THREADS - 1; i++) {
    threads[i]->join();
  }

  // Now add up the reachability over all of the threads, for each node
  vector<double> avgDeltaReachable_vec (n, 0.0); // takes node id to average reachability

  for (int i = 0; i < NUM_THREADS; i++) {
    // thread i
    for (int j = 0; j < n; j++) {
      // node j
      avgDeltaReachable_vec[j] += avgDeltaReachable[i][j];
    }
  }

  // delete the arrays with average reachability for each thread
  for (int i = 0; i < NUM_THREADS; i++) {
    delete[] avgDeltaReachable[i];
  }

  // Average
  for (int j = 0; j < n; j++) {
    avgDeltaReachable_vec[j] /= NUM_THREADS;
  }

  return avgDeltaReachable_vec;

}
// END

























//
