

// Mutate element X, return mutation
// Set mutated to true/false depending on whether something actually changed
Subset mutate(Subset& X, mt19937& gen, binomial_distribution<int>& count_dist,
  uniform_int_distribution<int>& elt_dist,
  InfluenceOracleRIS& f, vector<double>& costs, bool& mutated, double& threshold) {

  int n = costs.size();
  // We will do count element flips
  int count = count_dist(gen);
  mutated = (count > 0);

  // mutate X
  vector<bool> mut (n, false);
  bitset<GRAPH_SIZE> mut_elts = X.elts;
  double mut_c = X.c;

  int r; // the random id to mutate at each step
  for (int i = 0; i < count; i++) {
    r = elt_dist(gen); // random element
    while (mut[r]) {
      // already chosen
      r = elt_dist(gen);
    }

    mut_elts[r].flip();

    if (mut_elts[r]) mut_c += costs[r];
    else mut_c -= costs[r];

    mut[r] = true;
  }

  double mut_f = f(mut_elts, threshold);

  return Subset (mut_elts, mut_f, mut_c);

}



// Returns whether X1 is better than X2, both in bin bin
// r is the max bin number
// theta is the cost bound, -1 is infinity
bool isBetter(Subset& X1, Subset& X2, int bin, int r, double theta, double omega) {

  //cout << "Comparing ";
  //X1.print();
  //cout << " and ";
  //X2.print();
  //cout << endl << flush;

  if ((theta >= 0) && (X1.c <= theta) && (X2.c > theta)) {
    // X2 is above the cost bound, while X1 is not
    return true;
  }
  else if ((bin == 0) || (bin == r)) {
    // Only compare based on cost
    if (X1.c < X2.c) return true;
    else return false;
  }
  else {
    if (X1.c/log(omega/(omega - X1.f)) < X2.c/log(omega/(omega - X2.f))) return true;
    else return false;
  }

}



// Returns whether X1 is better than X2,
// Just compares based on cost effectiveness
bool isBetterCE(Subset& X1, Subset& X2) {

  if (X1.c/X1.f < X2.c/X2.f) return true;
  else return false;

}



// Run the pareto optimization with bins algorithm
// theta = -1 if cost bound if infinity
// Put warm_start as true in order to add the subset warm to the bins initially
// is_max is whether we are doing the max version of the problem or not
ParetoState ParetoOptimizationBins(InfluenceOracleRIS& f, vector<double>& costs,
  double omega, double delta, int r, double theta, int T, bool warm_start,
  vector<Subset>& warm_start_sets, bool cost_effective_compare, ofstream& output, double greedyt,
  double greedyf, double greedyc, bool is_max) {

  // Size of S
  int n = costs.size();

  // Sources of randomness
  mt19937 gen;
  random_device rd;
  gen.seed(rd());

  // Random distribution for how many elements to mutate
  binomial_distribution<int> count_dist (n, 1.0/n);
  uniform_int_distribution<int> elt_dist (0, n-1);

  // Create the bins
  Bins bins (omega, delta, r, n, costs, gen);

  if (warm_start) {
    // Add the warm start sets
    for (int i = 0; i < warm_start_sets.size(); i++) {
      bins.replaceBin(warm_start_sets[i], bins.findBin(warm_start_sets[i]), gen);
    }
  }
  //bins.print();

  // This will keep track of the state
  ParetoState state (greedyt, greedyf, greedyc);

  // Mutations for T steps
  cout << "\r" << setw(10) << 0 << "%" << flush;
  for (int t = 0; t < T; t++) {

    // Uniformly randomly choose from among the bins that have an element in them
    int bin = bins.randomBin(gen);

    // Mutate the element in the selected bin
	 bool mutated;
    Subset X_mut = mutate(bins.elements[bin], gen, count_dist, elt_dist, f, costs, mutated, omega);

    // And find what bin the new element is in (if it is not the same f value)
    int mut_bin;
    // first do a local search
    bool found = bins.findBinLocal(X_mut, bin, mut_bin);

    if (!found) {
      // binary search
      mut_bin = bins.findBin(X_mut);
    }

    //cout << mut_bin - mut_bin_2 << ",";

    // Put the mutation into the bin if it is better, or if the bin is empty
    if (!bins.elements_set[mut_bin]) {
      // No element in that bin
      bins.replaceBin(X_mut, mut_bin, gen);
    }
    else if (!cost_effective_compare && isBetter(X_mut, bins.elements[mut_bin], mut_bin, r, theta, omega)) {
      // Is better than current element in bin
      bins.replaceBin(X_mut, mut_bin, gen);
    }
    else if (cost_effective_compare && isBetterCE(X_mut, bins.elements[mut_bin])) {
      // Is better than current element in bin
      bins.replaceBin(X_mut, mut_bin, gen);
    }

    //bins.print();

    if (t%100 == 0) {
     	cout << "\r" << setw(10) << int(100*(double(t)/T)) << "% ";
       if (is_max) {
         //state.forwardMax(t, bins, theta);
       }
       else{
         state.forwardCover(t, bins);
       }

       state.printCurrent();
       cout << flush;
       //bins.print();

 	 }

   if (t%10000 == 0) {
       state.write("easc", t, output);
   }

  }

  cout << endl;

  return state;

}




// Run the pareto optimization algorithm (with no bins)
// theta = -1 if cost bound if infinity
// is_max is whether we are doing the max or cover version of problem
ParetoState ParetoOptimization(InfluenceOracleRIS& f, vector<double>& costs,
  double omega, double theta, int T, ofstream& output, double greedyt,
  double greedyf, double greedyc, bool is_max) {

  // Size of S
  int n = costs.size();

  // Sources of randomness
  mt19937 gen;
  random_device rd;
  gen.seed(rd());

  // Random distribution for how many elements to mutate
  binomial_distribution<int> count_dist (n, 1.0/n);
  uniform_int_distribution<int> elt_dist (0, n-1);

  // Create the pareto pool
  ParetoPoolCover pool (n);

  // This will keep track of the state
  ParetoState state (greedyt, greedyf, greedyc);

  // Mutations for T steps
  cout << "\r" << setw(10) << 0 << "%" << flush;
  for (int t = 0; t < T; t++) {

	 //pool.print(output);

    // Uniformly randomly choose a subset in the pool
    Subset X = pool.randomElt(gen);
	 //output << "Random element ";
	 //X.print(output);
	 //output << " chosen." << endl;

    // Mutate the selected element
	 bool mutated;
   Subset X_mut = mutate(X, gen, count_dist, elt_dist, f, costs, mutated, omega);

    //output << ((X_mut.c < X.c) || (X_mut.f > X.f)) << endl;
	 if (mutated) pool.add(X_mut);
	 //if (mutated) pool.add(X_mut, output);
	 //if ((X_mut.c < X.c) || (X_mut.f > X.f)) pool.add(X_mut, output);
	 //if ((X_mut.c < X.c) || (X_mut.f > X.f)) pool.add(X_mut);

	  if (t%10000 == 0) {

       // Update the pareto history
       Subset soln;
       if (!is_max && pool.minCost(soln, omega)) {
          // there is an element
          state.forward(t, soln);
       }
       //else if (is_max && pool.maxF(soln, theta)) {
          // there is an element
      //    state.forward(t, soln);
      // }

    	 cout << "\r" << setw(10) << int(100*(double(t)/T)) << "% ";

       state.printCurrent();
       cout << flush;
       state.write("pom", t, output);

       cout << " pool size: " << pool.size() << "   ";
    	 cout << flush;
	  }

  }

  cout << endl;

  return state;

}






















//
