
// Represents a subset of S
class Subset {

public:

  // The elements in this set
  bitset<GRAPH_SIZE> elts;


  // The f value and cost of this set
  double f;
  double c;



  // Represents no subset
  // Default for bins
  Subset() {}



  Subset(const Subset& other) {

	  this->elts = other.elts;
	  this->f = other.f;
	  this->c = other.c;

  }



  // Empty set
  // n elements in S
  Subset(int n): f(0.0), c(0.0) {

     for (int i = 0; i < GRAPH_SIZE; i++) {
	     this->elts[i] = false;
	  }
	  
  }



  // input all data for subset
  Subset(bitset<GRAPH_SIZE>& elts, double f, double c):
    elts(elts), f(f), c(c) { }



  // Print info about this subset to the screen
  void print(ostream& output) {

    /**cout << "{";
    for (int i = 0; i < this->elt_ids.size(); i++) {
      if (i != 0) cout << ",";
      cout << this->elt_ids[i];
    }
    cout << "}";
    */

    output << " f=" << this->f;
    output << " c=" << this->c;

	 output << " {";
	 for (int i = 0; i < GRAPH_SIZE; i++) {
		if (this->elts[i]) output << i <<",";
	 }
	 output << "} ";

  }


};














//
