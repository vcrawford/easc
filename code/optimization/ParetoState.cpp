


// Represents a single step of the pareto optimization algorithm
class ParetoStep {

public:

  // the step number
  int t;

  // the f value we keep track of
  double f;

  // and the cost
  double c;

  ParetoStep(int t, double f, double c): t(t), f(f), c(c) {}

};



// Records information over the duration of the pareto optimization
// algorithm for submodular cover
class ParetoState {

public:


  // The step information
  // Only record step when element in last bin has changed
  vector<ParetoStep> steps;


  // the last step that was written to a file
  //int last_written;


  // For the greedy algorithm
  double greedyt;
  double greedyf;
  double greedyc;


  ParetoState() {}


  // No history to start with
  // Input the number of f evaluations (greedyt) f value (greedyf) and cost (greedyc) of the
  // greedy solution
  ParetoState(double greedyt, double greedyf, double greedyc): greedyt(greedyt),
    greedyf(greedyf), greedyc(greedyc) {} // last_written(-1) {}



  // The pareto algorithm moved forward an iteration
  void forwardCover(int t, Bins& bins) {

    // Update the last bin history if a new element is in the last bin
    if (bins.elements_set[bins.r]) {
      // last element is set
      if ((this->steps.size() == 0) || (this->steps.back().c != bins.elements[bins.r].c)) {
        // new data point
        this->steps.push_back(ParetoStep(t, bins.elements[bins.r].f, bins.elements[bins.r].c));
      }
    }

  }



  // The pareto algorithm moved forward an iteration
  void forward(int t, Subset& mincost) {

    if ((this->steps.size() == 0) || (this->steps.back().c != mincost.c)) {
      // new data point
      this->steps.push_back(ParetoStep(t, mincost.f, mincost.c));
    }
  }


  // Write values of last in this->steps but for time step t
  // Input what to divide each term by (the greedy values)
  // name is what to label this data
  void write(string name, int t, ofstream& output) {

    if (this->steps.size() == 0) return;

    //for (int i = this->last_written+1; i < this->steps.size(); i++) {
    output << name << " ";
    output << t/this->greedyt << " ";
    output << this->steps.back().f/this->greedyf << " ";
    output << this->steps.back().c/this->greedyc << endl;
    //}

    //this->last_written = this->steps.size()-1;

  }



  // Print the last step's info
  void printCurrent() {

    if (this->steps.size() != 0) {
      cout << "(t=" << this->steps.back().t/this->greedyt;
      cout << " f=" << this->steps.back().f/this->greedyf;
      cout << " c=" << this->steps.back().c/this->greedyc << ")   ";
    }
    else {
      cout << "(--)";
    }
  }

};










//

















//
