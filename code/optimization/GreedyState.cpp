// Handles the state of the greedy algorithm
// Answers queries about what is the most cost-effective element
// Marginal gains and costs are input, doesn't directly deal with those functions
class GreedyState {


public:


  // Takes each node id to whether that node has been chosen or not
  bitset<GRAPH_SIZE> chosen;

  vector<int> chosen_ids;


  // The value of F at the end of every iteration
  // The last one is the current value of F
  vector<double> f_vals;


  // Current value of F (also the last entry in f_vals)
  double current_f;


  // Current cost
  double current_cost;



  // Start out having picked nothing
  GreedyState() { }



  // Add an element to the greedy solution
  // Chooses the best element in terms of cost-effectiveness according to F
  // that is within the budget (if a budget exists - set to -1.0 if none exists)
  // Returns the id of the chosen element
  // Must provide the marginal gains of adding each element
  // Updates alpha and betas
  int chooseElt(vector<double>& gains, vector<double>& costs, double budget) {

    // This will be set to the most cost-effective
    int best = 0;
    double best_ce = gains[best]/costs[best];

    // Also need to keep track of worst gain for beta for this round
    double worst_gain = gains[best];

    // Look at every node
    for (int i = 1; i < this->chosen.size(); i++) {

      if (this->chosen[i]) continue; // already picked this element

      // can't fit this element within the budget
      if ((budget != -1) && (this->current_cost + costs[i] > budget)) continue;

      // Comparing with node i

      // Check if it's the most cost-effective
      double compare_ce = gains[i]/costs[i];

      if (compare_ce <= best_ce) {
        // Element not better
      }
      else {
        // This element is better
        best = i;
        best_ce = compare_ce;
      }
    }

    // Add the best to the greedy solution
    this->chosen[best] = true;
    this->chosen_ids.push_back(best);
    this->current_f = this->current_f + gains[best];
    this->current_cost = this->current_cost + costs[best];
    this->f_vals.push_back(this->current_f);

    return best;

  }



  // Can we not add a new element and stay within the budget?
  // If true, the max version of greedy is complete
  bool budgetExhausted(vector<double>& costs, double budget) {

    if (budget == -1) return false;

    for (int i = 0; i < this->chosen.size(); i++) {
      // Can we add node i?
      if (!this->chosen[i] && (costs[i] + this->current_cost <= budget)) {
        return false;
      }
    }

    return true;
  }



  // Write the greedy data to an output stream
  void write(ofstream& output) {

    // The different f values
    output << "f:";
    for (int i = 0; i < this->f_vals.size(); i++) {
      if (i != 0) output << "," << this->f_vals[i];
      else output << this->f_vals[i];
    }
    output << " ";

  }



};








































//
