



// Run the greedy algorithm for submodular cover with costs
GreedyState greedyCover(InfluenceOracleRIS& f, vector<double>& costs, double& threshold,
	ofstream& output) {

  // Holds all info for greedy algorithm, picks elements with max marginal gain
  GreedyState state;

  // Holds the marginal gains for each iteration
  vector<double> gains;

  cout << "\r" << setw(10) << 0 << "%" << flush;

  //bool not_set = true;
  // Keep adding elements until we have gotten above our threshold
  while (state.current_f < threshold) {

    gains = f.deltaF(state.chosen, state.current_f, threshold);

	 //if (not_set) {
	 //	for (int i = 0; i < gains.size(); i++) output << gains[i] << " ";
	 //	output << endl;
    //}

    int chosen = state.chooseElt(gains, costs, -1);

	 //not_set = false;
    cout << "\r" << setw(20) << "f=" << state.current_f << "(";
	 cout << int(100*(state.current_f/threshold)) << "%)           " << flush;

	 //output << state.current_f << ",";
	 //output << flush;
  }

  //output << endl;

  cout << endl;

  return state;

}

































//
