
/**
 * Represents a pool of pareto solutions
 * No binning or anything like that
 */
class ParetoPoolCover {

public:

  // The current pool of solutions in order of increasing c, increasing f
  list<Subset> pool;


  ParetoPoolCover(int n) {

    // Add empty set to pool
    Subset emptyset (n);
    this->add(emptyset);

	 //emptyset.print(cout);
	 //cout << endl;

	 //(*this->pool.begin()).print(cout);
	 //cout << endl;
  }



  // Add element to pool if there is not already a better solution
  // Remove worse solutions
  void add(Subset& elt) {

    list<Subset>::iterator it = this->pool.begin();

    // iterate through lower cost c
    // If there is a better solution, it will be in here
    while ((it != this->pool.end()) && (*it).c < elt.c) {
      if ((*it).f >= elt.f) {
        // This solution is at least as good, shouldn't add
        return;
      }
      it++;
    }

    // There wasn't a better solution
    it = this->pool.insert(it, elt);
    it++;

    // Now go through higher cost elements and remove any worse ones
    while (it != this->pool.end()) {
      if ((*it).f <= elt.f) {
        // This solution is worse
        it = this->pool.erase(it);
        continue;
      }
      it++;
    }

  }




  // Add element to pool if there is not already a better solution
  // Remove worse solutions
  void add(Subset& elt, ofstream& output) {

	 output << "Adding element ";
	 elt.print(output);
	 output << endl;
    list<Subset>::iterator it = this->pool.begin();

    // iterate through lower cost c
    // If there is a better solution, it will be in here
    while ((it != this->pool.end()) && (*it).c <= elt.c) {
      if ((*it).f >= elt.f) {
        // This solution is at least as good, shouldn't add
		  (*it).print(output);
		  output << " is better." << endl;	  
        return;
      }
      it++;
    }

    // There wasn't a better solution
    it = this->pool.insert(it, elt);
    it++;
	 output << "Added." << endl;

    // Now go through higher cost elements and remove any worse ones
    while (((*it).f == elt.f) && (it != this->pool.end())) {
      // This solution is worse
		output << "Remove ";
		(*it).print(output);
		output << endl;
      it = this->pool.erase(it);
    }

  }



  // Find the minimum cost set in the pool that has f value above threshold
  // Returns whether one exists
  bool minCost(Subset& best_soln, double threshold) {

    for (list<Subset>::iterator it = this->pool.begin(); it != this->pool.end(); it++) {
      if ((*it).f >= threshold) {
        best_soln = *it;
        return true;
      }
    }

    return false;

  }


  // Return a random pool element
  Subset randomElt(mt19937& gen) {

      uniform_int_distribution<int> dist (0, this->pool.size()-1);

      int k = dist(gen);

      list<Subset>::iterator it = this->pool.begin();

      for (int i = 0; i < k; i++) {
        it++;
      }

		return *it;

  }



  int size() {
    return this->pool.size();
  }



  void print(ofstream& output) {

    for (list<Subset>::iterator it = this->pool.begin(); it != this->pool.end(); it++) {
      (*it).print(output);
      output << ",";
    }

    output << endl;
  }

};
















//
