
/**
 * Represents a pool of pareto solutions
 * No binning or anything like that
 */
class ParetoPool {

public:

  // The current pool of solutions in order of increasing c
  list<Subset> pool;

  // The max cost
  double theta;

  ParetoPool(int n, double theta): theta(theta) {

    // Add empty set to pool
    Subset emptyset (n);
    this->add(emptyset);

  }



  // Add element to pool if there is not already a better solution
  // Remove worse solutions
  void add(Subset& elt) {

    // don't add things above twice the budget
    if ((this->theta >= 0) && (elt.c > 2*this->theta)) return;

    bool should_add = true; // whether we should add the element to the back at the end
    list<int>::iterator add_f = this->f_order.end(); // where to add the f value
    list<int>::iterator add_c = this->c_order.end(); // where to add the c value

    // Look for a match on f
    for (list<int>::iterator it = this->f_order.begin(); it != this->f_order.end(); it++) {
      if (this->pool[*it].f > elt.f) {
        add_f = it;
        break;
      }
      else if (this->pool[*it].f == elt.f) {
        // There is a match on f
        if (this->pool[*it].c > elt.c) {
          // Should add this in place of the other one
          this->replaceMatchF(*it, elt);
        }
        should_add = false;
        break;
      }
    }

    // Look for a match on c
    for (list<int>::iterator it = this->c_order.begin(); it != this->c_order.end(); it++) {
      if (this->pool[*it].c > elt.c) {
        add_c = it;
        break;
      }
      if (this->pool[*it].c == elt.c) {
        // There is a match on c
        if (this->pool[*it].f < elt.f) {
          // Should add this in place of the other one
          this->replaceMatchC(*it, elt);
        }
        should_add = false;
        break;
      }
    }

    if (should_add) {
      this->pool.push_back(elt);
      this->f_order_it.push_back(this->f_order.insert(add_f, this->pool.size()-1));
      this->c_order_it.push_back(this->c_order.insert(add_c, this->pool.size()-1));
    }

  }



  // Find the minimum cost set in the pool that has f value above threshold
  // Returns whether one exists
  bool minCost(Subset& best_soln, double threshold) {

    for (list<int>::iterator it = this->c_order.begin(); it != this->c_order.end(); it++) {
      if (this->pool[*it].f >= threshold) {
        best_soln = this->pool[*it];
        return true;
      }
    }

    return false;

  }



   // Find the max f in the pool that has cost beneath budget
   // Returns whether one exists
   bool maxF(Subset& best_soln, double budget) {

     list<int>::iterator it = this->f_order.end();

     while (it != this->f_order.begin()) {

       it--;

       if (this->pool[*it].c <= budget) {
         best_soln = this->pool[*it];
         return true;
       }
     }

   }



  // Return a random pool element
  Subset randomElt(mt19937& gen) {

      uniform_int_distribution<int> dist (0, this->pool.size()-1);

      return this->pool[dist(gen)];
  }



  int size() {
    return this->pool.size();
  }



  void print() {

    for (list<int>::iterator it = this->f_order.begin(); it != this->f_order.end(); it++) {
      cout << " " << this->pool[*it].f;
    }

    cout << endl;

    for (list<int>::iterator it = this->c_order.begin(); it != this->c_order.end(); it++) {
      cout << " " << this->pool[*it].c;
    }

    cout << endl;
  }

};
















//
