// Bins structure for the pareto optimization with bins algorithm
// Holds subsets of a ground set in each bin
// Starts with just an empty set in the first bin
class Bins {


public:


  // bin parameters
  double omega;
  double delta;
  int r;


  // Holds subset for each bin
  vector<Subset> elements;


  // Maps bin number to whether bin has an element in it
  vector<bool> elements_set;


  // bins that have elements in them
  // Note that we never have to remove from here since
  // bins never become empty after being filled
  vector<int> set_bins;

  // Random sequence of bin numbers, sampled from uniform distribution
  // Some bins may be empty, will need to cycle through these
  vector<int> random_seq;


  // The lower bound for each of the bins
  vector<double> bin_bounds;


  // distribution for picking random bins
  uniform_int_distribution<int> dist;


  // omega is the max value of f in the bins
  // delta and r determine the regions of each bin in [0,omega]
  // n is the number of elements in S
  // Begins with empty set in bin 0
  Bins (double omega, double delta, int r, int n, vector<double> costs, mt19937& gen):
    elements(r+1, Subset()), elements_set(r+1, false), omega(omega), delta(delta), r(r),
    dist(0, r) {

    // Compute the lower bound values for each bin
    this->computeLowerBounds();

    // Add empty set to bin 0
    Subset emptyset (n);
    this->replaceBin(emptyset, 0, gen);

	 //emptyset.print(cout);
	 //cout << endl;

    // And the full set to bin r
    //Subset fullset (n, omega, costs);
    //this->replaceBin(fullset, r);

  }



  // Computes the lower bound values for each bin
  void computeLowerBounds() {

    for (int i = 0; i <= this->r; i++) {
      this->bin_bounds.push_back((1-pow(this->delta, i))*this->omega);
    }

  }



  // Search for bin of X between l and h (inclusive)
  // Assumes that X is in fact between l and h
  int findBin(Subset& X, int l, int h) {

    if (l == h) return l;

    int m = (l + h)/2;

    if (X.f < this->bin_bounds[m+1]) return this->findBin(X, l, m);
    else return this->findBin(X, m+1, h);


  }



  // Find which bin X is in using a binary search
  int findBin(Subset& X) {

    return this->findBin(X, 0, this->r);
  }


  // Find which bin a subset is mapped to
  // Give starting point which is believed to be "close"
  // Stores bin in bin if found
  // Linear search radiating out from starting point
  // Returns true if found inside the local search bound
  bool findBinLocal(Subset& X, int start, int& bin) {

    // How many steps away from start we look
    int i = 0;

    while (i <= LOCAL_SEARCH_BOUND) {

      if (((start + i) <= this->r) && this->mapsToBin(X, start+i)) {
        bin = start+i; // found the bin
        return true;
      }

      if (((start - i) >= 0) && this->mapsToBin(X, start-i)) {
        bin = start-i; // found the bin
        return true;
      }

      i++;
    }

    // Wasn't found
    return false;
  }



  // Does f value of X map to bin?
  bool mapsToBin(Subset& X, int bin) {

    if ((bin == this->r) && (X.f >= this->bin_bounds[this->r])) {
      return true;
    }
    else if ((X.f >= this->bin_bounds[bin]) && (X.f < this->bin_bounds[bin+1])) {
      return true;
    }
    else {
      return false;
    }

  }



  // Put subset into bin
  bool replaceBin(Subset& X, int bin, mt19937& gen) {

    /**if (bin == r) {
      cout << "Replacing bin " << bin << ": ";
      X.print();
      cout << endl;
    }*/

    if (!this->elements_set[bin]) {
      // This bin is having an element added for the first time
      this->elements_set[bin] = true;
      this->set_bins.push_back(bin);
    }

    this->elements[bin] = X;

  }



  // Clear out and fill the random sequence of bins
  // Number generated is proportionate to how many of the bins added to are filled
  void fillRandom(mt19937& gen) {

    this->random_seq.clear();

    for (int i = 0; i < RANDOM_SEQUENCE; i++) {
      this->random_seq.push_back(this->dist(gen));
    }

  }



  // Return the number of a uniformly random bin that has an
  // element in it
  int randomBin(mt19937& gen) {

    if (this->random_seq.empty()) {
      this->fillRandom(gen);
    }

    int bin = this->random_seq.back();
    this->random_seq.pop_back();

    while (!this->elements_set[bin]) {

     if (this->random_seq.empty()) {
        this->fillRandom(gen);
      }


      bin = this->random_seq.back();
      this->random_seq.pop_back();

    }

    return bin;

  }



  // Print info about the bins to the screen
/**  void print() {

    cout << "=== BINS ===" << endl;
    for (int i = 0; i < this->r; i++) {

      if (this->elements_set[i]) {
        cout << "[" << (1-pow(this->delta, i))*this->omega << "," << (1-pow(this->delta, i+1))*this->omega  << "]: ";
        this->elements[i].print();
        cout << endl;
      }
    }

    if (this->elements_set[this->r]) {
      cout << "last bin [" << (1-pow(this->delta, this->r))*this->omega << "," << this->omega  << "]: ";
      this->elements[this->r].print();
      cout << endl;
    }

  }
*/

};






















//
