# ParetoOptimization

Experiments with pareto optimization algorithms for submodular cover.
Constants must be set in the file Constants.h.

## Submodular cover
The program is compiled by running
```
g++ -O3 -std=c++11 ExperimentCover.cpp -o ExperimentCover -pthread
```
Run the program like
```
./ExperimentCover $graph $n $weight $cost $threshold $T $epsilon $output $warm_start $ce_compare $bins $oracle $oracle_num
```
where
* graph is the name of the graph data file
* n is the number of nodes in the graph
* weight is what the starting weight of edges should be (it's then divided by degree)
* cost is the file that has the cost for each node
* threshold is for the cover instance
* T times the number of f evaluations of greedy is how many times we will let to pareto algorithm run
* epsilon is the (multiplicative) feasibility guarantee
* $output is where the output of the program should be written
* $warm_start is 1 or 0 depending on whether we want to start with the greedy solution in the last bin
* $ce_compare is 1 or 0 depending on whether we want to use the cost-effectiveness comparison operator instead
  of the one in the paper
* bins is 1 or 0 depending on whether we want to run pareto optimization with bins
* $oracle is the file where the f oracle is saved (saves reachable sets)
* $oracle_num is how many samples from the oracle should be used to approximate f (max 100,000)
