#include <list>
#include <vector>
#include <iostream>
#include <cassert>
#include <map>
#include <string>
#include <fstream>
#include <cmath>
#include <chrono>
#include <thread>
#include <future>
#include <random>
#include <algorithm>
#include <bitset>

using namespace std;
using namespace chrono;

#include "Constants.h"
#include "../influenceoracle/InfluenceOracle.h"
#include "../optimization/SubmodularOptimization.h"

int main(int argc, char** argv) {


  cout << "=== RUNNING EXPERIMENTS ===" << endl;

  /** Read in and check all parameters **/
  cout << "Reading in input parameters ..." << endl;

  string graph_filename = string(argv[1]);
  int n = stoi(argv[2]);
  double weight = stof(argv[3]);
  string cost_filename = string(argv[4]);
  double threshold = stof(argv[5]);
  int T = stoi(argv[6]); // number of greedy runs to run pareto optimization for
  double epsilon = stof(argv[7]);
  string output_file = string(argv[8]); // write output of experiments here
  bool warm_start = (stoi(argv[9]) == 1); // whether to initially add greedy solutions (at each step) to the pool
  bool cost_effective_compare = (stoi(argv[10]) == 1); // compare with ce instead of the proven one
  bool bins = (stoi(argv[11]) == 1); // whether to use bins algorithm or unbounded pareto pool
  string oracle_file = argv[12]; // where to read in/write the influence oracle
  int oracle_num = stoi(argv[13]); // Number of rr sets


  cout << "Parameters successfully read in." << endl;

   ofstream output;
   output.open(output_file);


   /** Build the influence graph**/

   cout << "Building influence graph from file " << graph_filename << "..." << endl;
   ProbabilisticGraph graph (graph_filename, n, false, weight, false);
   cout << "Influence graph built." << endl;



   // Read in the costs of each node

   cout << "Reading in costs from file " << cost_filename << "..." << endl;

   vector<double> costs;
   ifstream cost_strm;
   cost_strm.open(cost_filename);

   double c_min = -1.0; // and the minimum

   double cost;
   while (cost_strm >> cost) {
     costs.push_back(cost);
     if ((c_min == -1.0) || (cost < c_min)) c_min = cost;
   }

   cost_strm.close();

   // Make sure graph and costs line up
   if (costs.size() != n) {
     cout << "There are " << n << " nodes in the graph, while there are only "
          << costs.size() << " costs given." << endl;
     cerr << "=== TERMINATE ===" << endl;
     exit(EXIT_FAILURE);
   }

   cout << "Costs read in." << endl;


   // Build RIS influence oracle

   cout << "Building the RIS influence oracle..." << endl;
   InfluenceOracleRIS f_ris;
   ifstream input (oracle_file);
   if (input.good()) {
     f_ris.init(oracle_file, oracle_num);
   }
   else {
     f_ris.init(graph, oracle_num);
	  f_ris.save(oracle_file);
   }


   /** Run the greedy algorithm**/
	double infeasible_threshold = (1-epsilon)*threshold;
   cout << "Running the greedy algorithm for submodular cover with threshold " << infeasible_threshold << " ..." << endl;
   GreedyState state = greedyCover(f_ris, costs, infeasible_threshold, output);
   cout << " f=" << state.current_f;
   cout << " c=" << state.current_cost;
   cout << " |A|=" << state.chosen_ids.size() << endl;
   cout << "Greedy algorithm is complete." << endl;

	cout << "Sanity check ..." << endl;
	InfluenceOracle f (graph, 10000);
	vector<bool> chosen_vec;
	for (int i = 0; i < state.chosen.size(); i++) chosen_vec.push_back(state.chosen[i]);
	cout << "MC oracle says f=" << f(chosen_vec, state.chosen_ids) << endl;

   // Run the pareto with bins optimization algorithm and write solution in final bin over time
   cout << "Running the pareto optimization with bins algorithm for " << T*state.chosen_ids.size()*n << " steps with ";
   cout << "delta=" << 1-c_min/state.current_cost << " and r=" << int(log(epsilon)/log(1-c_min/state.current_cost));
	cout << "... " << endl;

   // Get the greedy solution for warm up
   vector<Subset> warm_start_sets;
   bitset<GRAPH_SIZE> greedy_bitset;
   double c = 0.0;
   for (int i = 0; i < state.chosen_ids.size(); i++) {
     // build the bitset with this id in it
     greedy_bitset[state.chosen_ids[i]] = true;
     c += costs[state.chosen_ids[i]];
     warm_start_sets.push_back(Subset (greedy_bitset, state.f_vals[i], c));
   }



   // Pareto algorithm
   ParetoState pareto_state;
   time_t start = time(0);
   if (bins) {
     pareto_state = ParetoOptimizationBins(f_ris, costs, threshold, 1-c_min/state.current_cost,
       int(log(epsilon)/log(1-c_min/state.current_cost)), -1, T*state.chosen_ids.size()*n,
       warm_start, warm_start_sets, cost_effective_compare, output, state.chosen_ids.size()*n, state.current_f,
       state.current_cost, false);
   }
   else {
     pareto_state = ParetoOptimization(f_ris, costs, (1-epsilon)*threshold, -1,
	    T*state.chosen_ids.size()*n, output, state.chosen_ids.size()*n, state.current_f,
		 state.current_cost, false);
   }
   time_t end = time(0);
   cout << "The pareto optimization algorithm ran in " << end-start << " seconds." << endl;
   output.close();


   cout << "=== EXPERIMENTS COMPLETE ===" << endl;


}





































//
