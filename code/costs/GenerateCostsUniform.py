# Generate n costs and write to a file

import sys
import numpy as np
import numpy.random as rn

n = sys.argv[1]
l = sys.argv[2]
h = sys.argv[3]
filename = sys.argv[4]

print "Computing " + n + " random costs with between " + l + " and " + h
print "Writing to file " + filename

# get n random numbers
rand = rn.uniform(float(l),float(h),int(n))

# write them to the file
file = open(filename, 'w')
for elt in rand:
    file.write(str(elt) + " ")
file.close()
