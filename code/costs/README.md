This library generates costs for vertices in a social network. The costs for
each vertex are written as one line, space separating each cost, in order of
vertex id.

# GenerateCosts.py
Costs are generated for each node randomly by sampling from a normal distribution.
This does not depend on the network, and so does not need the network as input.
Run like
```
python GenerateCosts.py $n $mu $s $output
```
where
* $n is how many vertices we need
* $mu is the mean of the vertices
* $s is the standard deviation
* $output is where the costs should be written

# GenerateCostsDegree.py
Costs for each node are their outgoing degree times multiplicative noise (1-x) where x is sampled from a folded
normal distribution with mean 0.0 (so the cost can only decrease from the degree).
Min cost is capped at 1.0.
Run like
```
python GenerateCostsDegree.py $graph $output $s
```
where
* $graph is the social network in the format used by the graph library
* $output is where the costs should be written
* $s is the standard deviation of the normal distribution

# GenerateCostsUniform.py
Costs are selected uniformly randomly. Run like
```
python GenerateCostsUniform.py $l $h $output
```
where
* $n is how many vertices we need
* $l is the lower bound
* $h is the higher bound
* $output is where the costs should be written
