# Generate n costs and write to a file
# The cost of each node is its outgoing degree, plus some random noise
# python GenerateCostsDegree.py graphfilename outputfilename

import sys
import numpy as np
import numpy.random as rn
import math

graph_file = sys.argv[1]
output_file = sys.argv[2]
v = sys.argv[3]

# open the graph file
graph = open(graph_file, 'r')

# takes the id of each node to its degree
# the degree of a node is its number of outgoing edges
degrees = {}

for line in graph:
    [x_str,y_str] = line.split()
    x = int(x_str)
    y = int(y_str)

    if x not in degrees:
        degrees[x] = 0

    if y not in degrees:
        degrees[y] = 0

    degrees[x] = degrees[x] + 1

graph.close()

# To perturb by random noise
rand = rn.normal(0.0,float(v),len(degrees))

# Write them to the file
output = open(output_file, 'w')
for id in range(len(degrees)):
    if id in degrees:
        rand_cost = 1.0 + (1 + abs(rand[id]))*degrees[id]
        output.write(str(rand_cost) + " ")
    else:
        output.write("1.0 ")
output.close()
















#
