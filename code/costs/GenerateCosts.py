# Generate n costs and write to a file
# Sampled from a normal distribution with mean mu, std dev v
# Costs are always > 0
# python GenerateCosts.py n mu v filename

import sys
import numpy as np
import numpy.random as rn

n = sys.argv[1]
mu = sys.argv[2]
v = sys.argv[3]
filename = sys.argv[4]

print "Computing " + n + " random costs with mean " + mu + " and standard deviation " + v
print "Writing to file " + filename

# get n random numbers
rand = rn.normal(float(mu),float(v),int(n))

# make sure they are all positive
for elt in np.nditer(rand, op_flags=['readwrite']):
    while elt <= 0:
        elt[...] = rn.normal(float(mu),float(v))

# write them to the file
file = open(filename, 'w')
for elt in rand:
    file.write(str(elt) + " ")
file.close()
