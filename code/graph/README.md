This is a simple graph library.

## Testing
The following programs are to aid in testing.
### Testing the realizations of probabilistic graphs
#### ComputeRealizations.cpp
Compute a bunch of realizations based on a probabilistic graph.
In order to compile, run
```
make computerealizations
```

Run like
```
./ComputeRealizations <n> <graph> <realize> <num>
```
where
* <graph> holds the probabilistic graph that we sample realizations from
* <n> is the number of nodes in the graph
* <realize> is the file that realizations will be written to
* <num> is how many realizations will be generated

#### ComputeGraphFromRealizations.py
This script looks at a file of realizations and computes what the probabilistic graph is
expected to be. Run like
```
python ComputeGraphFromRealizations.py <realizations> <output>
```
where
* <realizations> is the file containing the realized graphs, each ending with a blank line
* <output> is where the expected graph should be written to

#### TestRealizations.bash
The script TestRealizations.bash computes a random weighted ER graph, computes n realizations
based upon that graph, and then computes the expected probabilistic graph based upon those
realizations.

### Testing reachability computations

#### ComputeReachable.cpp
Given a probabilistic graph, computes a realization and then a set of reachable nodes
on that realization.
In order to compile, run
```
make computereachable
```

Run like
```
./ComputeReachable <n> <graph> <reachable> <k>
```
where
* <graph> is the probabilistic graph
* <n> is the number of nodes in the graph
* <reachable> is where the reachable nodes should be written to
* <k> the reachable nodes are reachable from node 0,...,k-1

#### TestReachable.bash
The script TestReachable.bash tests the reachability computations. Produces an image file
showing a reachable component of a graph realization.

### PrintWeights.cpp
Test the weights being assigned to an input graph. Run like
```
./PrintWeights.out <graph> <n> <w> <normalize>
```
where
* graph is the file name where the graph data is
* n is the number of nodes in the graph
* w is the uniform weight we assign to all edges
* normalize is 1 or 0 depending on whether we want to then normalize by the degree
  (the weight of edge (a,b), which is w, is divided by the incoming degree of b)
## Helper Scripts
The following helper scripts are provided:

### VisualizeGraph.py
This script outputs a dot file that is a visualization of the graph. There is also the option
to highlight a subset of nodes. Run like:
```
python VisualizeGraph.py <graph> <dot> <highlight>
```
where
* <graph> is the input graph file to visualize
* <dot> is where we should write the dot file
* <highlight> is a text file with the ids of nodes that should be highlighted, separated by
commas. This input is optional.

To convert the dot file into a png file, run:
```
dot -Tps <dot> -o <image>
```
where
* <dot> is the input dot file
* <image> is where the image file should be saved

### PrepareDataset.py
Takes a real data set and puts it in the appropriate format for the graph library. In particular,
it gets rid of any lines that start with '#', ensures that ids are in 0,...,n-1, gets
rid of self edges. Run as
follows:
```
python PrepareDataset.py <input> <output> <directed>
```
where
* <input> is the dataset to prepare
* <output> is where the new data will be written
* <directed> is whether the input graph has directed edges or not (if not, each input edge is
  written twice, for each direction)

### CheckWeightedGraph.py
This takes in a weighted graph and computes some of its properties. The output is
number of nodes, number of edges, average degree, average edge weight, and edge
weight standard deviation. Run as follows:
```
python CheckWeightedGraph.py <graph>
```
where
* <graph> is the file where the graph data is

There is also CheckGraph.py, which does the same except on a graph without weights, and
does not output any data having to do with weights.

### RemoveHighestDegreeNodes.py
Removes the node with the highest outgoing degree, and then writes a new graph file.
Run like
```
python RemoveHighestDegreeNodes.py <graph> <output>
```
where
* graph is the file with the graph data
* output is where we will write the graph

### RemoveIDs.py
Removes all nodes with id at least a certain value.
Run like
```
python RemoveHighestDegreeNodes.py <graph> <output> <N>
```
where
* graph is the file with the graph data
* output is where we will write the graph
* N is the id we start filtering out

### RemoveEvenIDs.py
Removes nodes with even degree.
Run like
```
python RemoveHighestDegreeNodes.py <graph> <output>
```
where
* graph is the file with the graph data
* output is where we will write the graph
