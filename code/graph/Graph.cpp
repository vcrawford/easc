// Represents a directed graph

using namespace std;

// Represents an edge from the perspective of a node
class Neighbor {

  int id; // the id of the neighbor
  double weight; // the weight of the edge

public:

  Neighbor() {}

  Neighbor(int id, double weight): id(id), weight(weight) {}

  int getID() {
    return this->id;
  }

  double getWeight() {
    return this->weight;
  }

  // For normalizing by incoming degree
  // Divides weight by divide_by
  void divideWeight(float divide_by) {
    this->weight = this->weight/divide_by;
  }

};

class Graph {

protected:
     // Every edge is stored twice

     // vector that takes the ith node to its outgoing neighbors
     // still has entry even if no neighbors
     vector< vector<Neighbor> > neighbors_out;

     // And incoming neighbors
     vector< vector<Neighbor> > neighbors_in;


     bool validEdge(int x, int y) {
       return ((this->validID(x)) && (this->validID(y)) && (x != y));
     }


     bool validID(int x) {
       return (x >= 0);
     }


     // Add an edge that goes from node x to node y
     // If already an edge, does nothing
     // Returns whether edge was actually added
     bool addEdge(int x, int y) {

       if (this->edgeExists(x,y)) return false;
       else {
         this->neighbors_out[x].push_back(Neighbor(y, 1.0));
         this->neighbors_in[y].push_back(Neighbor(x, 1.0));
         return true;
       }
     }

   public:


      Graph() {}


      Graph(int n): neighbors_out(n, vector<Neighbor>()), neighbors_in(n, vector<Neighbor>()) {}


      // Pass in the outgoing neighbors
      // Computes the mirroring neighbors in
      Graph(vector< vector<Neighbor> >& neighbors_out): neighbors_out(neighbors_out),
        neighbors_in(neighbors_out.size(), vector<Neighbor>()) {

        for (int i = 0; i < this->neighbors_out.size(); i++) {
          // the outgoing neighbors of node i
          for (int j = 0; j < this->neighbors_out[i].size(); j++) {
            // this edge is from i to this->neighbors_out[i][j]
            this->neighbors_in[this->neighbors_out[i][j].getID()].push_back(Neighbor(i, 1.0));
          }
        }

      }


      // Read the edges from a file for a graph of size n nodes
      // Assumes the graph is encoded as a series of rows where pairs of node ids
      // represent a directed edge: "x y" is an edge from x to y
      Graph(const string& filename, int n): neighbors_out(n, vector<Neighbor>()),
        neighbors_in(n, vector<Neighbor>()) {

         ifstream file;
         file.open(filename.c_str());

         string line;
         while (getline(file, line)) {

            stringstream line_strm;
            line_strm.str(line);

            // read in the node ids
            int x, y;
            line_strm >> x;
            line_strm >> y;

            if (!this->validEdge(x,y)) {
              cout << "(" << x << "," << y << ") is not a valid edge" << endl;
              cerr << "=== TERMINATE ===" << endl;
              exit(EXIT_FAILURE);
            }

            if (!this->addEdge(x,y)) {
              // This edge already existed
              cout << "Warning: There are repeated edges in " << filename << endl;
            }
         }

         file.close();

      }


      // Prints the outgoing neighbors of each node to the screen
      void printGraph() {

        for (int i = 0; i < this->neighbors_out.size(); i++) {

           cout << setw(3) << i << ": ";

           for (int j = 0; j < this->neighbors_out[i].size(); j++) {
              cout << "(" << i << "," << this->neighbors_out[i][j].getID() << ")";
           }

           cout << endl;
        }
      }


      // Prints edges in graph to an output stream
      void writeEdges(ofstream& out) {

        for (int i = 0; i < this->neighbors_out.size(); i++) {
          for (int j = 0; j < this->neighbors_out[i].size(); j++) {
            out << i << " " << this->neighbors_out[i][j].getID() << endl;
          }
        }
      }



      // Gets the nodes that this node has an incoming edge from
      vector<Neighbor> getNeighborsIn(const int& node) const {

         return this->neighbors_in[node];
      }



      // Gets the nodes that this node has an outgoing edge to
      vector<Neighbor> getNeighbors(const int& node) const {

         return this->neighbors_out[node];
      }


      // Checks whether there exists an edge from node i to node j
      bool edgeExists(const int& i, const int& j) {

        for (int k = 0; k < this->neighbors_out[i].size(); k++) {
          if (this->neighbors_out[i][k].getID() == j) return true;
        }

        return false;
      }


      // Get the count of nodes in the graph
      int getNodeCount() const {

         return this->neighbors_out.size();
      }


      // Find reachable nodes from a set of nodes
      // Input is a membership vector, as well as a id vector of the starting nodes
      // Gives both a membership vector and an id vector of reachable nodes
      // Breadth-first search
      void getReachable(const vector<bool>& nodes, const vector<int>& node_ids,
        vector<bool>& reachable_nodes, vector<int>& reachable_node_ids) {

        // These will be what are reachable
        // Start with initial nodes
        reachable_nodes = nodes;
        reachable_node_ids = node_ids;

        // Queue for breadth first search
        queue<int> search;
        // Add the initial nodes to search
        for (int i = 0; i < node_ids.size(); i++) {
          search.push(node_ids[i]);
        }

        // Breadth first search
        while (!search.empty()) {

          // Node we are seaching
          int search_node = search.front();
          search.pop();

          // Get the search node's neighbors
          vector<Neighbor> neighbors = this->getNeighbors(search_node);

          // Go through the neighbors
          for (int j = 0; j < neighbors.size(); j++) {

            int neighbor_id = neighbors[j].getID();

            if (!reachable_nodes[neighbor_id]) {
              // new node
              reachable_nodes[neighbor_id] = true;
              reachable_node_ids.push_back(neighbor_id);
              search.push(neighbor_id);
            }
          }
        }

      }

      // Count the number of nodes in the graph that are reachable from node
      // but were not reachable from existing nodes
      int countNewReachable(int node, vector<bool>& existing) {

        // Check if node is in existing, then there will be no new nodes
        if (existing[node]) return 0;

        vector<bool> new_nodes (this->getNodeCount(), false);
        new_nodes[node] = true;
        vector<int> new_node_ids;
        new_node_ids.push_back(node);

        // Queue for breadth first search
        queue<int> search;
        search.push(node);

        // Breadth first search
        while (!search.empty()) {

          // Node we are seaching
          int search_node = search.front();
          search.pop();

          // Get the search node's neighbors
          vector<Neighbor> neighbors = this->getNeighbors(search_node);

          // Go through the neighbors
          for (int j = 0; j < neighbors.size(); j++) {

            int neighbor_id = neighbors[j].getID();

            if (!new_nodes[neighbor_id] && !existing[neighbor_id]) {
              // new node
              new_nodes[neighbor_id] = true;
              new_node_ids.push_back(neighbor_id);
              search.push(neighbor_id);
            }
          }
        }

        return new_node_ids.size();

      }


      // Count the number of nodes reachable from a single node
      int countReachable(int node) {

        vector<bool> nodes (this->getNodeCount(), false);
        nodes[node] = true;
        vector<int> node_ids;
        node_ids.push_back(node);

        vector<bool> reachable_nodes;
        vector<int> reachable_node_ids;

        this->getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

        return reachable_node_ids.size();
      }

      // Count the number of nodes reachable from a vector of nodes
      int countReachable(vector<bool>& nodes, vector<int>& node_ids) {

        vector<bool> reachable_nodes;
        vector<int> reachable_node_ids;

        this->getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

        return reachable_node_ids.size();
      }


      // Return a set of all nodes that are reachable from a set of nodes
      // breadth-first search
      vector<bool> getReachable(vector<bool>& nodes, vector<int>& node_ids) {

        vector<bool> reachable_nodes;
        vector<int> reachable_node_ids;

        this->getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

        return reachable_nodes;
      }


      // Return a set of all nodes that are reachable from a node
      // breadth-first search
      vector<bool> getReachable(int node) {

        vector<bool> nodes (this->getNodeCount(), false);
        nodes[node] = true;
        vector<int> node_ids;
        node_ids.push_back(node);

        vector<bool> reachable_nodes;
        vector<int> reachable_node_ids;

        this->getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

        return reachable_nodes;
      }


      // Return a set of all nodes that are reachable from a node
      // breadth-first search
      vector<int> getReachableIDs(int node) {

        vector<bool> nodes (this->getNodeCount(), false);
        nodes[node] = true;
        vector<int> node_ids;
        node_ids.push_back(node);

        vector<bool> reachable_nodes;
        vector<int> reachable_node_ids;

        this->getReachable(nodes, node_ids, reachable_nodes, reachable_node_ids);

        return reachable_node_ids;
      }


      // Get the incoming edge degree of every node
      // Returns a vector that takes id to degree
      vector<int> getIncomingDegrees() {

        vector<int> degrees (this->getNodeCount(), 0);

        for (int i = 0; i < this->neighbors_in.size(); i++) {
          degrees[i] = this->neighbors_in[i].size();
        }

        return degrees;
      }

};



































//
