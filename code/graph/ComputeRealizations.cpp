// Computes a number of realizations for a graph, and then prints them to a
// file
#include "Graph.h"

int main(int argc, char** argv) {

  string graph_file = argv[1];
  int n = stoi(argv[2]);
  string realization_file = argv[3];
  int num_realizations = stoi(argv[4]);

  ProbabilisticGraph pg (graph_file, n);

  ofstream output;
  output.open(realization_file);

  for (int i = 0; i < num_realizations; i++) {
    Graph g = pg.sampleRealization();
    g.writeEdges(output);
    output << endl;
  }

  output.close();

}
