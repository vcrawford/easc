// Represents a graph that has a weight assigned to each edge
// The edges are directed

using namespace std;

class WeightedGraph: public Graph {



     bool validWeight(double w) {
       return (w >= 0);
     }



     // Add an edge that goes from node x to node y with weight w
     // If already an edge, does nothing
     // Returns whether edge was actually added
     bool addEdge(int x, int y, double w) {

       if (this->edgeExists(x,y)) return false;
       else {
         this->neighbors_out[x].push_back(Neighbor(y, w));
         this->neighbors_in[y].push_back(Neighbor(x, w));
         return true;
       }
     }



   public:



      // Read the edges from a file
      // has_weights is whether the weights should be read from the file "x y w"
      // w is a uniform weight we should assign if we are not reading in weights
      // Assign uniform weight w to every edge
      // normalize is whether every edge from a to b should be divided by the
      // degree of b
      WeightedGraph(const string& filename, int n, bool has_weights, double w, bool normalize):
        Graph (n) {

        ifstream file;
        file.open(filename.c_str());

        string line;
        while (getline(file, line)) {

           stringstream line_strm;
           line_strm.str(line);

           // read in the node ids
           int x, y;
           line_strm >> x;
           line_strm >> y;

           if (!this->validEdge(x,y)) {
             cout << "(" << x << "," << y << ") is not a valid edge" << endl;
             cerr << "=== TERMINATE ===" << endl;
             exit(EXIT_FAILURE);
           }


           if (!has_weights) {

             if (!this->addEdge(x,y,w)) {
               cout << "Warning: There are repeated edges in " << filename << endl;
             }
           }
           else {
             // read in the weight
             double w_in;
             line_strm >> w_in;

             if (!this->validWeight(w_in)) {
               cout << w_in << " is not a valid edge weight" << endl;
               cerr << "=== TERMINATE ===" << endl;
               exit(EXIT_FAILURE);
             }

             if (!this->addEdge(x,y,w_in)) {
               cout << "Warning: There are repeated edges in " << filename << endl;
             }
           }

        }

        file.close();

        if (normalize) {
          // Now that we have all the edges, divide by degree
          // Weight of edge from a to b is divided by the degree of b
          this->normalizeWeights();
        }

      }



      // Normalize all edge weights by degree
      // An edge from a to b is divided by the incoming degree of b
      void normalizeWeights() {

        // The incoming degrees of every node
        vector<int> degrees = this->getIncomingDegrees();

        // Normalize weight for all edges stored as outgoing
        for (int i = 0; i < this->neighbors_out.size(); i++) {
          // outgoing edges of node i
          for (int j = 0; j < this->neighbors_out[i].size(); j++) {
            // edge from i to nei
            int nei = this->neighbors_out[i][j].getID();
            this->neighbors_out[i][j].divideWeight(degrees[nei]);
          }
        }

        // Normalize weight for all edges stored as incoming
        for (int i = 0; i < this->neighbors_in.size(); i++) {
          // outgoing edges of node i
          for (int j = 0; j < this->neighbors_in[i].size(); j++) {
            this->neighbors_in[i][j].divideWeight(degrees[i]);
          }
        }

      }



      // Prints the graph to the screen with weights
      void printWeightedGraph() {

        for (int i = 0; i < this->neighbors_out.size(); i++) {

           cout << setw(3) << i << ": ";

           for (int j = 0; j < this->neighbors_out[i].size(); j++) {

             Neighbor nei = this->neighbors_out[i][j];

             cout << "(" << i << "," << nei.getID() << "," << nei.getWeight() << ")";
           }

           cout << endl;
        }

      }

};





















//
