// A graph where realizations can be sampled from
// The edge weights represent edge existence probabilities

class ProbabilisticGraph: public WeightedGraph {


  bool validWeight(double w) {
    return ((w >= 0) && (w <= 1));
  }

public:

  ProbabilisticGraph(const string& filename, int n): WeightedGraph(filename, n, true, 0, false) {}


  // Assign weight to every edge
  // normalize is whether we also divide every edge from a to b
  // by the degree of b
  ProbabilisticGraph(const string& filename, int n, bool has_weights, double w,
    bool normalize): WeightedGraph(filename, n, has_weights, w, normalize) {}



  // Sample a reverse reachable set with node target
  bitset<GRAPH_SIZE> sampleRRSet(int target, mt19937& gen,
    uniform_real_distribution<double>& dist) {

      // These will be what are reverse reachable
      bitset<GRAPH_SIZE> reachable;
      reachable[target] = true;

      // Queue for breadth first search
      queue<int> search;
      search.push(target);

      // Breadth first search
      while (!search.empty()) {

        // Node we are searching
        int search_node = search.front();
        search.pop();

        // Get the search node's incoming neighbors
        vector<Neighbor> neighbors = this->getNeighborsIn(search_node);

        // Go through the neighbors
        for (int j = 0; j < neighbors.size(); j++) {

          Neighbor neighbor_node = neighbors[j];
          int neighbor_id = neighbor_node.getID();

          if (!reachable[neighbor_id]) {
            // found a new node
            // flip the "coin" for the edge
            double random_num = dist(gen);

            if (random_num < neighbor_node.getWeight()) {
              // reached
              reachable[neighbor_id] = true;
              search.push(neighbor_id);
            }
          }
        }
      }

      return reachable;
  }



  // Sample a set of nodes that are reachable from nodes
  // Breadth first search where a coin is flipped to cross an edge
  void sampleReachable(const bitset<GRAPH_SIZE>& nodes, const vector<int>& node_ids,
    bitset<GRAPH_SIZE>& reachable_nodes, vector<int>& reachable_node_ids, mt19937& gen,
    uniform_real_distribution<double>& dist) {

    // These will be what are reachable
    // Start with just the initial nodes
    reachable_nodes = nodes;
    reachable_node_ids = node_ids;

    // Queue for breadth first search
    queue<int> search;
    // copy initial nodes into search
    for (int i = 0; i < node_ids.size(); i++) {
      search.push(node_ids[i]);
    }

    // Breadth first search
    while (!search.empty()) {

      // Node we are searching
      int search_node = search.front();
      search.pop();

      // Get the search node's neighbors
      vector<Neighbor> neighbors = this->getNeighbors(search_node);

      // Go through the neighbors
      for (int j = 0; j < neighbors.size(); j++) {

        Neighbor neighbor_node = neighbors[j];
        int neighbor_id = neighbor_node.getID();

        if (!reachable_nodes[neighbor_id]) {
          // found a new node
          // flip the "coin" for the edge
          double random_num = dist(gen);

          if (random_num < neighbor_node.getWeight()) {
            // reached
            reachable_nodes[neighbor_id] = true;
            reachable_node_ids.push_back(neighbor_id);
            search.push(neighbor_id);
          }
        }
      }
    }

  }


  // Returns the number of nodes reachable in a random sample
  int sampleNumberReachable(const bitset<GRAPH_SIZE>& nodes, const vector<int>& node_ids,
    mt19937& gen, uniform_real_distribution<double>& dist) {

    bitset<GRAPH_SIZE> reachable_nodes;
    vector<int> reachable_node_ids;

    this->sampleReachable(nodes, node_ids, reachable_nodes, reachable_node_ids,
      gen, dist);

    return reachable_node_ids.size();

  }


  // Create a Graph that is a sample realization of this probabilistic graph
  // Assumes that edge weights are probabilities
  Graph sampleRealization(mt19937& gen, uniform_real_distribution<double>& dist) {

    // The neighbor vector for the realization
    vector< vector<Neighbor> > realization_neighbors (this->neighbors_out.size());

    // Sample every edge
    for (int i = 0; i < this->neighbors_out.size(); i++) {
      // Sampling neighbors of node i
      for (int j = 0; j < this->neighbors_out[i].size(); j++) {
        // sampling edge (i, neighbors[i][j])
        Neighbor neighbor = this->neighbors_out[i][j];

        double random_num = dist(gen);

        if (random_num < neighbor.getWeight()) {
          // this edge exists
          realization_neighbors[i].push_back(neighbor);
        }
      }
    }

    Graph realization (realization_neighbors);
    return realization;

  }
  // END

};

























//
