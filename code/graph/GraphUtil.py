
# Read in list of all edges from graph file
def readEdges(filename):

    graph = open(filename, 'r')

    edges = []

    for line in graph:
        [x,y] = line.split()
        x = int(x)
        y = int(y)

        edges.append((x,y))

    graph.close()

    return edges


# Computes a list that takes node id to its outgoing degree
# Need as input a list of edges
def getDegrees(edges):

    degrees = {}

    for edge in edges:

        [x,y] = edge

        if x not in degrees:
            degrees[x] = 0

        if y not in degrees:
            degrees[y] = 0

        degrees[x] = degrees[x] + 1

    return degrees


# Return the max degree and min degree of any node
def getExtremeDegrees(degrees):

    max_degree = -1
    min_degree = len(degrees)

    for key,val in degrees.iteritems():
        if val > max_degree:
            max_degree = val
        if val < min_degree:
            min_degree = val

    return [min_degree, max_degree]


# Return the id of the node with the highest degree
def getHighestDegreeID(degrees):


    max_degree = -1
    id = -1

    for i in range(len(degrees)):
        if degrees[i] > max_degree:
            max_degree = degrees[i]
            id = i

    return id


# Remove the node with a certain id from edges
def removeID(id, edges):

    new_edges = []

    for edge in edges:

        [x,y] = edge

        if (x!=id) and (y!=id):
            new_edges.append(edge)

    return new_edges

# Remove nodes with id >= id
# Return the new edge set
def removeHigherIDs(id, edges):

    new_edges = []

    for edge in edges:

        [x,y] = edge

        if (x < id) and (y < id):
            new_edges.append(edge)

    return new_edges

# Remove nodes with even ids
# Return the new edge set
def removeEvenIDs(edges):

    new_edges = []

    for edge in edges:

        [x,y] = edge

        if (x%2 == 1) and (y%2 == 1):
            new_edges.append(edge)

    return new_edges

# Re-assign the node ids so that they are between 0 and n-1
def reassignIDs(edges):

    # Takes old id to new id
    id_map = {}
    n = 0 # how many nodes we have seen so far

    # The edges with the new ids
    new_edges = []

    for edge in edges:

        [x,y] = edge

        if x not in id_map:
            id_map[x] = n
            n = n + 1
        if y not in id_map:
            id_map[y] = n
            n = n + 1

        new_edges.append((id_map[x], id_map[y]))

    return new_edges


# Write the edges to a graph file
def writeEdges(edges, filename):

    output = open(filename, "w")

    for edge in edges:

        [x,y] = edge

        output.write("{} {}\n".format(x,y))

    output.close()




















#
