graph="../code/evolutionaryalgs/runs/caheph/caheph.txt"
n=12006
weight=0.02
cost="../code/evolutionaryalgs/runs/caheph/caheph_costs.txt"
T=10
epsilon=0.05
output="../results/hepph/hepph_output"
warm_start=0
ce=0
s=0.5
oracle="../code/evolutionaryalgs/runs/caheph/caheph_oracle.txt"
oracle_num=30000
tau=970
plot="../results/hepph/hepph.pdf"
min_y=0.90
max_y=1.1


#./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output".txt" $warm_start $ce 1 $oracle $oracle_num

#exit 1

# bins experiments (EASC)
#for i in {1..10}
#do
#	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i".txt" $warm_start $ce 1 $oracle $oracle_num &
#done

# no bins experiments (POM)
#for i in {1..10}
#do
#	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i"_pom.txt" $warm_start $ce 0 $oracle $oracle_num &
#done

#wait

# concatenate the results
concat="cat "

for i in {1..10}
do
	concat=$concat$output$i".txt "
  concat=$concat$output$i"_pom.txt "
done

$concat > $output".txt"

python3 ../plot/PlotResults.py $output".txt" $plot 0 $T $min_y $max_y
