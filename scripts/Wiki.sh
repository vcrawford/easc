graph="../code/evolutionaryalgs/runs/wiki/wiki.txt"
n=7115
weight=0.04
cost="../code/evolutionaryalgs/runs/wiki/wiki_costs.txt"
T=40
epsilon=0.05
output="../results/wiki/wiki_output"
warm_start=0
ce=0
s=0.5
oracle="../code/evolutionaryalgs/runs/wiki/wiki_oracle.txt"
oracle_num=15000
tau=650
plot="../results/wiki/wiki.pdf"
min_y=0.88
max_y=1.04


#./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i".txt" $warm_start $ce 1 $oracle $oracle_num
#exit 1

# bins experiments (EASC)
for i in {1..10}
do
	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i".txt" $warm_start $ce 1 $oracle $oracle_num
done

# no bins experiments (POM)
for i in {1..10}
do
	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i"_pom.txt" $warm_start $ce 0 $oracle $oracle_num
done

wait

# concatenate the results
concat="cat "

for i in {1..10}
do
  concat=$concat$output$i".txt "
  concat=$concat$output$i"_pom.txt "
done

$concat > $output".txt"

python3 ../plot/PlotResults.py $output".txt" $plot 0 $T $min_y $max_y
