graph="../code/evolutionaryalgs/runs/cagrqc/cagrqc.txt"
n=5241
weight=0.07
cost="../code/evolutionaryalgs/runs/cagrqc/cagrqc_costs.txt"
T=30
epsilon=0.05
output="../results/grqc/grqc_output"
warm_start=0
ce=0
s=0.5
oracle="../code/evolutionaryalgs/runs/cagrqc/cagrqc_oracle.txt"
oracle_num=50000
tau=250
plot="../results/grqc/qrqc.pdf"
min_y=0.92
max_y=1.08


# bins experiments (EASC)
#for i in {1..10}
#do
#	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i"_4.txt" $warm_start $ce 1 $oracle $oracle_num
#done

# no bins experiments (POM)
#for i in {1..10}
#do
#	./ExperimentCover $graph $n $weight $cost $tau $T $epsilon $output$i"_4_pom.txt" $warm_start $ce 0 $oracle $oracle_num
#done

#wait

# concatenate the results
#concat="cat "

#for i in {1..10}
#do
#	concat=$concat$output$i"_4.txt "
#  concat=$concat$output$i"_4_pom.txt "
#done

#$concat > $output"_4.txt"

python3 ../plot/PlotResults.py $output".txt" $plot 0 $T $min_y $max_y
