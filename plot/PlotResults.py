# Plot the results of the experiments
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math as m
#import seaborn as sns

#sns.set()
#sns.set_style("whitegrid")
#sns.set_context("paper")

experiments_file = sys.argv[1]
plot_file = sys.argv[2]
min_x = float(sys.argv[3])
max_x = float(sys.argv[4])
min_y = float(sys.argv[5])
max_y = float(sys.argv[6])

# Takes algorithm name -> iteration number -> list of values
data = {}


# Read in results
input = open(experiments_file, "r")

for line in input:

    alg, t, f, c = line.split()
    t = float(t)
    c = float(c)

    if alg not in data:
        data[alg] = {}

    if t not in data[alg]:
        data[alg][t] = []

    data[alg][t].append(c);

input.close()


# Average the f values for every t
# Also compute standard deviation
results = {} # Takes algorithm name -> iteration number -> average
for alg, ts in data.items():
    for t, cs in ts.items():
        avg = 0.0
        for c in cs:
            avg = avg + c
        if alg not in results:
            results[alg] = {}
        results[alg][t] = avg/len(cs)

# Also compute standard deviation
std_dev = {} # Takes algorithm name -> iteration number -> standard deviation
for alg, ts in data.items():
    for t, cs in ts.items():
        sd = 0.0
        for c in cs:
            sd = sd + (c - results[alg][t])**2
        if alg not in std_dev:
            std_dev[alg] = {}
        std_dev[alg][t] = m.sqrt(sd/len(cs))


# Now plot all of the algorithms results
algs=['easc', 'pom']
titles={'easc':'EASC', 'pom':'POM'}
color={'easc':'c', 'pom':'m'}

plt.gcf().clear();
plt.rcParams['pdf.fonttype'] = 42;
plt.rcParams.update({'font.size': 24});
plt.rcParams.update({'font.weight': "bold"});
plt.rcParams["axes.labelweight"] = "bold";
plt.xlabel( 'Normalized evaluations' );
plt.ylabel( 'Normalized $c$ value' );
markSize=9

ts_list = [] # Will end up holding the x values

for alg in algs:

    ts_list = list(results[alg].keys())
    pi = int(len(ts_list)/5)
    ts_list.sort()
    Ys = []
    std_devs_low = []
    std_devs_high = []
    for x in ts_list:
        Ys.append(results[alg][x])
        #std_devs_low.append(results[alg][x] - 0.5*std_dev[alg][x])
        #std_devs_high.append(results[alg][x] + 0.5*std_dev[alg][x])
    plt.plot(ts_list, Ys, color=color[alg], linestyle='dashed',
        label=titles[alg], linewidth=3.0)
    #plt.plot(ts_list, Ys, color[alg], label=titles[alg], linestyle='dashed', linewidth=3)
    #plt.fill_between(ts_list, std_devs_low, std_devs_high, color=color[alg], alpha=0.2)

plt.plot(ts_list, len(ts_list)*[1.0], color='r', linestyle='dashed',
    label='greedy', linewidth=3.0)

plt.xlim( min_x, max_x );
plt.ylim( min_y, max_y );
yticks = list(np.arange(min_y, max_y, (max_y-min_y)/4))
plt.yticks(yticks);
xticks = list(np.arange(min_x, max_x, (max_x-min_x)/5))
plt.xticks(xticks);
plt.gca().grid(which='major', axis='both', linestyle='--')
plt.legend(loc='best', numpoints=1,prop={'size':18});
ax = plt.gca()
#ax.annotate('OPT', xy=(0, Opt), xytext=(-4.8, Opt), size=15)

#arrowprops=dict(facecolor='black', shrink=0.05) )
             #           )

plt.savefig( plot_file , format='pdf', bbox_inches='tight' );

































#
